//
//  Tengwar keyboard layout (based on FR-BÉPO)
//
//  Copyright (C) 2009-2015 Johan Winge
//  Copyright (C) 2023 Romain Taprest
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY;
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  This layout is part of the Free Tengwar Font Project. For more
//  information, see http://freetengwar.sourceforge.net/

xkb_symbols "basic"
{
    name[Group1] = "Tengwar CSUR BÉPO";

    // First row
    key <TLDE> { [           UE06C                                                 ] };
    key <AE01> { [           UE066,            UE071                               ] };
    key <AE02> { [           UE06A,            UE072,       U201C,           U2018 ] };
    key <AE03> { [           UE06B,            UE073,       U201D,           U2019 ] };
    key <AE04> { [           UE067,            UE074,   parenleft                  ] };
    key <AE05> { [           UE067,            UE075,  parenright                  ] };
    key <AE06> { [           UE049,            UE076,       UE057                  ] };
    key <AE07> { [           UE050,            UE077,       UE051                  ] };
    key <AE08> { [           UE068,            UE078                               ] };
    key <AE09> { [           U2044,            UE079                               ] };
    key <AE10> { [           U2E2D,            UE070,       UE07A                  ] };
    key <AE11> { [           UE069,         NoSymbol,       UE07B                  ] };
    key <AE12> { [           U200C,         NoSymbol,       UE07C                  ] };

    // Second row
    key <AD01> { [           UE005,          UE00D,          UE01D                 ] };
    key <AD02> { [           UE046,          UE02A,          UE047,          UE055 ] };
    key <AD03> { [           UE001,          UE009,          UE019                 ] };
    key <AD04> { [           UE04A,          UE050,          UE04B                 ] };
    key <AD05> { [           UE04C,          UE02B,          UE04D,          UE03D ] };
    key <AD06> { [           UE02E,          UE02C,          UE034                 ] };
    key <AD07> { [           UE00D,          UE036,          UE01D                 ] };
    key <AD08> { [           UE004,          UE00C,          UE01C                 ] };
    key <AD09> { [           UE022,          UE023                                 ] };
    key <AD10> { [           UE00E                                                 ] };
    key <AD11> { [           UE027,          UE026                                 ] };
    key <AD12> { [           UE015,          UE052,          UE031,          UE03D ] };

    // Third row
    key <AC01> { [           UE040,          UE032,          UE041,          UE056 ] };
    key <AC02> { [           UE053                                                 ] };
    key <AC03> { [           UE044,          UE05A,          UE045                 ] };
    key <AC04> { [           UE054                                                 ] };
    key <AC05> { [           UE060,          UE061,          comma,      semicolon ] };
    key <AC06> { [           UE003,          UE00B,          UE01B                 ] };
    key <AC07> { [           UE000,          UE008,          UE018                 ] };
    key <AC08> { [           UE025,          UE024                                 ] };
    key <AC09> { [           UE020,          UE014,          UE021                 ] };
    key <AC10> { [           UE010,          UE013,          UE012                 ] };
    key <AC11> { [           UE011,          UE03A,          UE03B                 ] };
    key <BKSL> { [           UE029,          UE039,          UE03C,          UE037 ] };

    // Fourth row
    key <LSGT> { [           UE006,          UE00E,          UE01E                 ] };
    key <AB01> { [           UE017                                                 ] };
    key <AB02> { [           UE016,          UE042,          UE043                 ] };
    key <AB03> { [           UE058,          UE059                                 ] };
    key <AB04> { [           UE063,          U2E2C,          U10FB,          UE062 ] };
    key <AB05> { [           UE002,          UE00A,          UE01A                 ] };
    key <AB06> { [      apostrophe,       question,         exclam,          UE065 ] };
    key <AB07> { [           UE00A                                                 ] };
    key <AB08> { [           UE007,          UE00F,          UE01F                 ] };
    key <AB09> { [           UE028,          UE02D,          UE057                 ] };
    key <AB10> { [           UE009,          NoSymbol,       UE019                 ] };
};

