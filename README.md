# Free Tengwar Font Project

This is a **fork** of the original Free Tengwar Font Project, located here :
https://sourceforge.net/p/freetengwar/code using SVN.

I made a copy of revision 257 to make this Git repository. Due to differences between
SVN and Git, the filetree has been changed.
